let initialState = {
    xucxacArray: 
    [
        {
        img: './imgGameXucXac/1.png',
        giaTri: 1,
        },
        {
        img: './imgGameXucXac/2.png',
        giaTri: 1,
        },
        {
        img: './imgGameXucXac/3.png',
        giaTri: 1,
        }
    ],
    option: null,
    result: null,
    winGame: 0,
    totalGame: 0,
};

export const xucxacReducer = (state = initialState, {type, payload}) => {
    switch (type) {
        case 'PLAY_GAME': {
            let arrayUpdated = state.xucxacArray.map((item) => {
                let random = Math.floor(Math.random() * 6) + 1;
                return {
                        img: `./imgGameXucXac/${random}.png`,
                        giaTri: random,
                        }
            });
            let sumGiaTri = arrayUpdated.reduce((totalNumber,item,index) => {return totalNumber += item.giaTri},0);
            console.log('bạn chọn: ', state.option)
            console.log('total number: ', sumGiaTri);
            if (sumGiaTri < 11 && state.option === 'XỈU'){
                state.result = 'WIN';
                state.winGame++;
                state.totalGame++;
            }else if (sumGiaTri >= 11 && state.option === 'TÀI'){
                state.result = 'WIN';
                state.winGame++;
                state.totalGame++;
            }
            else{
                state.result = 'LOSE'
                state.totalGame++;
            }
            console.log('kết quả: ', state.result);
            return {...state, xucxacArray: arrayUpdated}
        }
        case 'CHOOSE_OPTION': {
            return {...state, option: payload}
        }
        default:
            return state;
    }
}
