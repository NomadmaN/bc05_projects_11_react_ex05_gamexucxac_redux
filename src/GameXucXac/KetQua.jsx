import React, { Component } from 'react'
import { connect } from 'react-redux'

class KetQua extends Component {
  render() {
    return (
        <div>
          <div className="offset-4 col-4 offset-4">
              <button onClick = {this.props.handlePlayGame} className='btn btn-success fs-1 mt-5 p-2' style={{fontSize: '24px'}}>PLAY GAME</button>
          </div>
          <div className='col-12' style={{fontSize: '32px', fontWeight: 'bold'}}>YOU <span style={{color: 'blue', fontWeight: 'bold'}}>{this.props.finalResult}</span></div>
          {/* <h3>BẠN CHỌN: <span>XỈU</span></h3> */}
          <br></br>
          <h3>BẠN CHỌN: <span style={{color: 'blue', fontWeight: 'bold'}}>{this.props.chooseOption}</span></h3>
          <h4>Tổng số lần bạn thắng: <span style={{color: 'blue', fontWeight: 'bold'}}>{this.props.winGame}</span></h4>
          <h4>Tổng số tung xúc xắc: <span style={{color: 'blue', fontWeight: 'bold'}}>{this.props.totalGame}</span></h4>
        </div>
    )
  }
}

let mapStateToProps = (state) => {
  return {
    xucxacArray: state.xucxacReducer.xucxacArray,
    chooseOption: state.xucxacReducer.option,
    finalResult: state.xucxacReducer.result,
    winGame: state.xucxacReducer.winGame,
    totalGame: state.xucxacReducer.totalGame,
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    handlePlayGame: () => {
      console.log('handlePlayGame -> OK')
      const action = {
          type: 'PLAY_GAME',
      }
      dispatch (action);
    },
  
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(KetQua)