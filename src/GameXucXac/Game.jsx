import React, { Component } from 'react'
import { connect } from 'react-redux'

class Game extends Component {
    renderMangXucXac = () => {
        return this.props.xucxacArray.map((item) => {
            return (
            <img style={{width: "30%"}} src={item.img} alt='' />
            );
        });
    };
  render() {
    return (
      <div>
        <h1>GAME XÚC XẮC</h1>
        <h5 className='mb-5'>Luật game: Xỉu: tổng nhỏ hơn 11. Tài: tổng lớn hơn hoặc = 11</h5>
        <div className="row">
            <div className="col-4">
                {/* <button className='btn btn-danger fs-1 p-5'>XỈU</button> */}
                <button id='btnOption' onClick={()=> {this.props.handleChooseOption('XỈU')}} className='btn btn-danger fs-1 p-5' style={{fontSize: '38px'}}>XỈU</button>
            </div>
            <div className="col-4 align-item-center">
                {this.renderMangXucXac()}
            </div>
            <div className="col-4">
                {/* <button className='btn btn-danger fs-1 p-5'>TÀI</button> */}
                <button id='btnOption' onClick={()=> {this.props.handleChooseOption('TÀI')}} className='btn btn-danger fs-1 p-5' style={{fontSize: '38px'}}>TÀI</button>
            </div>
        </div>
      </div>
    )
  }
}


let mapStateToProps = (state) => {
    return {
        xucxacArray: state.xucxacReducer.xucxacArray,
    }
}

let mapDispatchToProps = (dispatch) => {
    return {
      handleChooseOption: (optionTaiXiu) => {
        console.log('handleChooseOption -> OK')
        const action = {
            type: 'CHOOSE_OPTION',
            payload: optionTaiXiu
        }
        dispatch (action);
      },
    
    }
  }

export default connect(mapStateToProps,mapDispatchToProps)(Game)