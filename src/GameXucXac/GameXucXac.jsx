import React, { Component } from 'react'
import KetQua from './KetQua'
import Game from './Game'
import bg_game from '../assets/bgGame.png'
import './game.css'

export default class GameXucXac extends Component {
  render() {
    return (
        <div className='font_game text-center' style={{backgroundImage: `url(${bg_game})`, width: '100vw', height: '100vh', backgroundSize: 'cover', backgroundPosition: 'bottom'}}>
            <div className='container py-5' >
                <Game></Game>
                <KetQua></KetQua>
            </div>
        </div>
    )
  }
}
